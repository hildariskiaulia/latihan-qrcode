package com.example.qrcode.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.qrcode.source.ScanRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val repo: ScanRepository
): ViewModel() {

    private val _state = MutableStateFlow(MainScreenState())
    val state = _state.asStateFlow()


    fun startScanning(){
        viewModelScope.launch {
            repo.startScanning().collect{data ->
                if (!data.isNullOrBlank()){
                    _state.value = state.value.copy(
                        details = data
                    )
                    loginToTv(data)
                }
            }
        }
    }

    fun loginToTv(qrCode:String){
        viewModelScope.launch {
            repo.loginAndroidTV(qrCode)

        }
    }
}