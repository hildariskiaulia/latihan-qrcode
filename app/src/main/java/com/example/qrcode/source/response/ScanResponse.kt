package com.example.qrcode.source.response

import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.flow.Flow

data class ScanResponse(
    val success : Boolean
)