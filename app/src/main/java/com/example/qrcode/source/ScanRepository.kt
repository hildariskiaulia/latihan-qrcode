package com.example.qrcode.source

import kotlinx.coroutines.flow.Flow

interface ScanRepository {
    fun startScanning(): Flow<String?>
    suspend fun loginAndroidTV(qrCode: String)
}