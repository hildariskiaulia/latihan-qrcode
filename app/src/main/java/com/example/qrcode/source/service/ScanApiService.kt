package com.example.qrcode.source.service

import com.example.qrcode.source.response.ScanResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ScanApiService {
    @FormUrlEncoded
    @POST("users/v4/link-account/android-tv")
    suspend fun loginAndroidTV(
        @Field("qr_code")qrCode: String,
        @Field("userToken")userToken: String,
    ) : ScanResponse
}